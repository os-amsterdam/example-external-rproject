A minimal example of how to create pages with alias in gitlab using Rmarkdown.

- gitlab: https://os-amsterdam.gitlab.io/example-external-rproject/

- alias: https://onderzoek.amsterdam.nl/extern/example-external-rproject/

---

# steps

This repro assumes that the system which you’re working on, has `R` and `Rstudio` installed. All the work has been tested for a `R` version of 4+. The example contains the following dependencies:

- rmarkdown
-	tidyverse
-	vegawidget

Aside from `rmarkdown`, the dependencies differ per project and are based on the R package which you're working with.

## step 1

Create a `Rmarkdown` which works on your local system and creates output in the form of a html file.

## step 2

Copy the folder `assets` from this repro into the same folder as where your `.Rmd` file is saved.

## step 3

Specify the styling aspects in the `yaml` on top of your `.Rmd` file. The style is specified with the following lines:

```{yaml}
output: 
  html_document:
    includes: 
      in_header: "assets/head.html"
    css: "assets/style/os-style.css"
```
The complete `yaml` of this example looks as follows

```{yaml}
---
title: "minimal Rmarkdown example"
date: "`r Sys.Date()`"
author: "Tobias Brils"
output: 
  html_document:
    includes: 
      in_header: "assets/head.html"
    toc: true
    toc_float:
      collapsed: false
    fig_caption: true
    css: "assets/style/os-style.css"
---
```

**Be sure** That all the indents and spacing are correct. `yaml` files are very sensitive to positioning.

## step 4

add a `.gitlab-ci.yml` file to your repro. This file lets `gitlab` know that you are creating a html file that you want to publish. This file again is very sensitive to indents and spacing. In order to publish your document, the file needs to be in the folder `public`. Making the folder `public` and adding documents to this folder happens in the `.gitlab-ci.yml` file. 

## step 4a

Publishing the document can be done in multiple ways. In this example a Docker is used to create the file. This has up and downsides. The big advantage is the possibility to create the file without issues of system dependencies. If this isn't important, we recommend not using a Docker (see `step 4b`). this is the `yaml` of this project:

```{yaml}
pages:
  image: rocker/verse
  
  before_script:
    - R -e "install.packages('ggiraph')"
  
  script:
    - Rscript -e "rmarkdown::render('index.Rmd', output_file = 'index.html')"
    - mv assets public
    - mv index.html public
  artifacts:
    paths:
      - public
```

## step 4b

Below is the same `yaml` without a Docker included:


```{yaml}
pages:
  stage: deploy
  script:
    - mv assets public
    - mv index.html public
  artifacts:
    paths:
      - public
```
